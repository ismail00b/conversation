﻿

(model.User.entityMethods.startConversation = function() {
	// Add your code here;
	var user = currentUser();
	
	var conversation = ds.User.query("conversations.users.ID = :1",user.ID);
	if(!conversation)
	{
		conversation = new ds.Conversation({name : this.username+" with "+user});
		conversation.save();
		
		var userByConv = new ds.UserByConversation({conversation : conversation, user : this});
		userByConv.save();
		
		var userByConv = new ds.UserByConversation({conversation : conversation, user : user});
		userByConv.save();
	}
		
}).scope="public";
