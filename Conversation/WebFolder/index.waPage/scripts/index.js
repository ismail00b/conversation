﻿
WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var start_Conversation = {};	// @button
	var currentUserEvent = {};	// @dataSource
// @endregion// @endlock

// eventHandlers// @lock

	start_Conversation.click = function start_Conversation_click (event)// @startlock
	{// @endlock
		var userEntity;
		ds.User.getEntity(sources.user.ID,
		{
			'onSuccess':function(event){
				userEntity = event.entity;
				userEntity.startConversation();
			},
			'onError':function(error){
			}
		});
		
		
	};// @lock

	currentUserEvent.onAttributeChange = function currentUserEvent_onAttributeChange (event)// @startlock
	{// @endlock
		// Add your code here
	};// @lock

// @region eventManager// @startlock
	WAF.addListener("start_Conversation", "click", start_Conversation.click, "WAF");
	WAF.addListener("currentUser", "onAttributeChange", currentUserEvent.onAttributeChange, "WAF");
// @endregion
};// @endlock
